# Import the actual program here
from movable_points import *
import os
import main_functions as mf
from flask import Flask, request, jsonify, abort, make_response, url_for, request, render_template, send_file
import json
import algorithm as algo
import cv2
import database as db
import time
from flask_cors import CORS
from database_library import database as dblib
from database_library import Base64Encoder
import pymongo
import base64
import bson

app = Flask(__name__)

cors = CORS(app,resources = {
    r"/*":{
        "origins":"*"
    }
})
db_handler = dblib()

# Show the output picture on /result
@app.route('/api/analyzer/result/<request_id>', methods=["GET"])
def getUserFeedbackImage(request_id):
    root_path = os.path.join('/tmp/custom_glove_api_data', request_id)
    if(not os.path.exists(root_path)):
        return jsonify({'Error': 'No feedback yet'})
    img_path = os.path.join(root_path, 'img')
    # Feedback result is stored as outline.jpg
    result_image = os.path.join(img_path, 'outline.jpg')
    if os.path.exists(result_image):
        return send_file(result_image, mimetype='image/jpg')
    return jsonify({'Error': 'No feedback yet'})


@app.route('/api/analyzer/measures/<request_id>', methods=["GET"])
def getMeasurements(request_id):
    # Get the root path to store measurements
    root_path = os.path.join('/tmp/custom_glove_api_data', request_id)
    if(not os.path.exists(root_path)):
        return jsonify({'Error': 'No feedback yet for'})
    # Save the measurement json in this path
    measures_path = os.path.join(root_path, 'measures')
    # measurement json
    result_json = os.path.join(measures_path, 'measures.json')
    if os.path.exists(result_json):
        data = None
        with open(result_json, 'r') as f:
            # Load the existing json file
            data = json.load(f)
        return data
    return jsonify({'Error': 'No feedback yet'})

# A test function


@app.route('/api/message/')
def get_message():
    return "this is my message"


@app.route('/api/analyzer/image/<request_id>', methods=['POST'])
def analyzer_image(request_id):

    # if request.method == 'POST':
    #     root_path = os.path.join(os.getcwd(), 'temp_storage')
    #     if not os.path.exists(root_path):
    #         os.mkdir(root_path)

    if request.method == 'POST':
        # init dB
        secret = 'mongodb+srv://gohara:1234@cluster0.hnle2.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'
        db_name = 'hands'
        collection_name = 'hands'
        user = time.strftime("%Y%m%d-%H%M%S")
        user = 'gohara'
        db_entry = db.Entry(secret, db_name, collection_name, user)

        root_path = os.path.join('/tmp/custom_glove_api_data', request_id)
        if os.path.exists(root_path):
            return jsonify({'Error': 'Invalid Request!'})
        # Create a new directory to store the input image and the outputs
        os.mkdir(root_path)
        # Create directories for image and measurement in root path
        img_path = os.path.join(root_path, 'img')
        measures_path = os.path.join(root_path, 'measures')
        os.mkdir(img_path)
        os.mkdir(measures_path)

        result_image = os.path.join(img_path, 'outline.jpg')
        result_json = os.path.join(measures_path, 'measures.json')


        if request.files:
            file_received = request.files["file"]
            if file_received.filename == '':
                return jsonify({'Error': 'Blank filename'})
            if file_received:

                # Read in the image
                image = request.files["file"]
                # Store the image to this path

                saved_path = os.path.join(img_path, 'test.jpg')
                image.save(saved_path)

                try:
                    hand = algo.run_algorithm(saved_path)
                    if type(hand) is dict:
                        # Write to dB
                        db_entry.add_original_img_error(saved_path)
                        db_entry.record_error(hand)
                        # If dict, is error message
                        return jsonify(hand)
                    else:
                        measurements = hand.measurements
                        cv2.imwrite(os.path.join(
                            img_path, 'outline.jpg'), hand.annotated)
                        # measurements = mf.get_measurements(saved_path)
                        # Save the measurement json to local
                        # Insert database code?
                        measures_path = os.path.join(root_path, 'measures')
                        with open(os.path.join(measures_path, 'measures.json'), 'w')as f:
                            json.dump(measurements, f)
                        # Delete the local image after processing
                        os.remove(saved_path)
                        db_entry.add_measurements(hand.measurements)
                        db_entry.add_original_img(hand.resized)
                        db_entry.add_annotated_img(hand.annotated)
                        return measurements
                except Exception as err:
                    return jsonify({'Error': err.args})
            else:
                return jsonify({'Error': 'This file type is not accepted. Please upload png or jpg'})
    else:
        return jsonify({'Error': 'This file type is not accepted. Please upload png or jpg'})
    return jsonify({'Error':'Unknown Error'})
# API method to store login information


@app.route('/api/login', methods=['POST'])
def storeLoginInfo():
    if request.method != 'POST':
        return jsonify({'Error': 'Not a post request'})
    content = request.json
    data_dir = os.path.join(os.getcwd(), 'data')
    saved_path = os.path.join(data_dir, 'loginInfo.json')
    with open(saved_path, 'w') as f:
        json.dump(content, f)
    return "Login info saved!"

# API method to store signup information


@app.route('/api/signup', methods=['POST'])
def storeSignupInfo():
    if request.method != 'POST':
        return jsonify({'Error': 'Not a post request'})
    content = request.json
    data_dir = os.path.join(os.getcwd(), 'data')
    saved_path = os.path.join(data_dir, 'SignupInfo.json')
    with open(saved_path, 'w') as f:
        json.dump(content, f)
    return "Signup info saved!"

@app.route('/api/data/measure/',methods=["GET"])
def getAllMeasurements():
    result = dict()
    try:
        num_match,data,features_list = db_handler.query_all(return_features=False)
        if num_match!=0:
            result_data = dict()
            for i in range(len(data)):
                for key in data[i].keys():
                    if (isinstance(data[i][key],bytes)):
                        data[i][key] = base64.b64encode(data[i][key]).decode('utf-8')
                    if (isinstance(data[i][key],bson.ObjectId)):
                        data[i][key] = str(data[i][key])
                result_data[i] = data[i]
            result['data'] = result_data
        result['num_match'] = num_match
        result_str = json.dumps(result) 
        return jsonify(result)
    except Exception as ex:
        return jsonify({'error':str(ex)})
#Query measurements for a given name
@app.route('/api/data/measure/<name>',methods=["GET"])
def getMeasurementsByName(name):
    result = dict()
    try:
        #num_match,data = db.query_data_by_feature('Name',name,exact=False)
        num_match,data = db_handler.query_data_by_feature('_id',name,exact=False)
        if num_match!=0:
            result_data = dict()
            for i in range(len(data)):
                for key in data[i].keys():
                    if (isinstance(data[i][key],bytes)):
                        data[i][key] = base64.b64encode(data[i][key]).decode('utf-8')
                    if (isinstance(data[i][key],bson.ObjectId)):
                        data[i][key] = str(data[i][key])
                result_data[i] = data[i]
            result['data'] = result_data
        result['num_match'] = num_match
        result_str = json.dumps(result) 
        return jsonify(result)
    except Exception as ex:
        return jsonify({'error':str(ex)})

#Query all hand measurements
@app.route('/api/data/measure/hand/',methods=["GET"])
def getAllHandMeasurements():
    result = dict()
    try:
        num_match,data,features_list = db_handler.query_all(return_features=False)
        hand_feature_names = ['pinky_length', 'ring_finger', 'middle_finger', 'index_finger', 'thumb', 'palm_to_pinky_ring', 'palm_to_ring_middle', 'palm_to_middle_index', 'wrist', 'knuckles', 'index_finger_width', 'middle_finger_width', 'pinky_width', 'thumb_width', 'ring_finger_width']

        if num_match!=0:
            result_data = dict()
            for i in range(len(data)):
                hand_item = dict()
                hand_item['_id'] = str(data[i]['_id'])
                #hand_item['Name'] = data[i]['Name']
                if 'fail' in data[i].keys():
                    hand_item['fail'] = 1
                else:
                    hand_item['fail'] = 0
                for key in hand_feature_names:
                    if key in data[i].keys():
                        hand_item[key] = data[i][key]
                    else:
                        hand_item[key] = None
                result_data[i] = hand_item
            result['data'] = result_data
        result['num_match'] = num_match
        result_str = json.dumps(result) 
        return jsonify(result)
    except Exception as ex:
        return jsonify({'error':str(ex)})
#Query hand measurements by name
@app.route('/api/data/measure/hand/<name>',methods=["GET"])
def getHandMeasurementsByName(name):
    result = dict()
    try:
        #num_match,data = db.query_data_by_feature('Name',name,exact=False)
        num_match,data = db_handler.query_data_by_feature('_id',name,exact=False)
        hand_feature_names = ['pinky_length', 'ring_finger', 'middle_finger', 'index_finger', 'thumb', 'palm_to_pinky_ring', 'palm_to_ring_middle', 'palm_to_middle_index', 'wrist', 'knuckles', 'index_finger_width', 'middle_finger_width', 'pinky_width', 'thumb_width', 'ring_finger_width']

        if num_match!=0:
            result_data = dict()
            for i in range(len(data)):
                hand_item = dict()
                hand_item['_id'] = str(data[i]['_id'])
                #hand_item['Name'] = data[i]['Name']
                if 'fail' in data[i].keys():
                    hand_item['fail'] = 1
                else:
                    hand_item['fail'] = 0
                for key in hand_feature_names:
                    if key in data[i].keys():
                        hand_item[key] = data[i][key]
                    else:
                        hand_item[key] = None
                result_data[i] = hand_item
            result['data'] = result_data
        result['num_match'] = num_match
        result_str = json.dumps(result) 
        return jsonify(result)
    except Exception as ex:
        return jsonify({'error':str(ex)})
#Query original image by name
@app.route('/api/data/measure/OriginalImage/<name>',methods=["GET"])
def getOrigImageByName(name):
    result = dict()
    try:
        #num_match,data = db.query_data_by_feature('Name',name,exact=False)
        num_match,data = db_handler.query_data_by_feature('_id',name,exact=False)
        if num_match!=0:
            result_data = dict()
            for i in range(len(data)):
                hand_item = dict()
                hand_item['_id'] = str(data[i]['_id'])
                #hand_item['Name'] = data[i]['Name']
                if 'fail' in data[i].keys():
                    hand_item['fail'] = 1
                else:
                    hand_item['fail'] = 0
                key = 'original'
                if (key in data[i].keys()):
                    #hand_item[key] = data[i][key] = base64.b64encode(data[i][key]).decode('utf-8')
                    hand_item[key] = base64.b64encode(data[i][key]).decode('utf-8')
                else:
                    hand_item[key] = None
                result_data[i] = hand_item
            result['data'] = result_data
        result['num_match'] = num_match
        result_str = json.dumps(result) 
        return jsonify(result)
    except Exception as ex:
        return jsonify({'error':str(ex)})
#Query original image by name
@app.route('/api/data/measure/AnnotatedImage/<name>',methods=["GET"])
def getAnnotatedImageByName(name):
    result = dict()
    try:
        #num_match,data = db.query_data_by_feature('Name',name,exact=False)
        num_match,data = db_handler.query_data_by_feature('_id',name,exact=False)
        if num_match!=0:
            result_data = dict()
            for i in range(len(data)):
                hand_item = dict()
                hand_item['_id'] = str(data[i]['_id'])
                #hand_item['Name'] = data[i]['Name']
                if 'fail' in data[i].keys():
                    hand_item['fail'] = 1
                else:
                    hand_item['fail'] = 0
                key = 'annotated'
                if (key in data[i].keys()):
                    hand_item[key] = data[i][key] = base64.b64encode(data[i][key]).decode('utf-8')
                    #hand_item[key] = data[i][key]
                    
                else:
                    hand_item[key] = None
                result_data[i] = hand_item
            result['data'] = result_data
        result['num_match'] = num_match
        result_str = json.dumps(result) 
        return jsonify(result)
    except Exception as ex:
        return jsonify({'error':str(ex)})

if __name__ == '__main__':
    
    app.run(debug=True,threaded=True)
