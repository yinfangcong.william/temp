import math 
import numpy as np
from scipy.spatial.distance import euclidean

def angle_between_vectors(v1, v2):
    # Vectors in format [(x1,y1), (x2,y2)]
    # magnitude1 = math.sqrt(((v1[0][0]-v1[1][0])**2) + ((v1[0][1]-v1[1][1])**2))
    # magnitude2 = math.sqrt(((v2[0][0]-v2[1][0])**2) + ((v2[0][1]-v2[1][1])**2))
    # print(magnitude1)
    # print(magnitude2)

    vector_1 = [v1[0][0]-v1[1][0], v1[0][1]-v1[1][1]]
    vector_2 = [v2[0][0]-v2[1][0], v2[0][1]-v2[1][1]]
    unit_vector_1 = vector_1 / np.linalg.norm(vector_1)
    unit_vector_2 = vector_2 / np.linalg.norm(vector_2)
    dot_product = np.dot(unit_vector_1, unit_vector_2)
    angle = np.arccos(dot_product)
    angle = math.degrees(angle)
    return angle


def adjusted_scale(length_pixel_per_cm, width_pixel_per_cm, length_pts, width_pts, vector):
    angle_bw_len = angle_between_vectors(length_pts, vector)
    cos_len = abs(math.cos(math.radians(angle_bw_len)))
    angle_bw_width = angle_between_vectors(width_pts, vector)
    cos_width = abs(math.cos(math.radians(angle_bw_width)))
    magnitude = euclidean(vector[0], vector[1])
    length_component = (cos_len*magnitude) / length_pixel_per_cm
    width_component = (cos_width*magnitude) / width_pixel_per_cm
    measurement = math.sqrt((length_component**2) + (width_component**2))
    return measurement

if __name__ == '__main__':
    v1 = [(204.0, 249.0), (50.0, 249.0)]
    v2 =  [(204.0, 249.0), (204.0, 153.0)]
    angle_between_vectors(v1, v2)