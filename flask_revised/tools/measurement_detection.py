import cv2
from scipy.spatial.distance import euclidean
import numpy as np
from error_code import ErrorCode

def midpoint(p1, p2):
    return ((p1[0]+p2[0])/2, (p1[1]+p2[1])/2)


def normal_points(p1, p2, px, num_points):
    # Returns two vectors of points normal to the line made by p1 & p2 at starting loc, px
    try:
        m = (p2[1]-p1[1]) / (p2[0] - p1[0])
        m = -1/m
    except:
        m = 0
    x1 = np.arange(px[0], px[0]+num_points, 1)
    x2 = np.arange(px[0], px[0]-num_points, -1)
    y1 = np.zeros(len(x1))
    y2 = np.zeros(len(x2))
    y1[0] = p1[1]
    y2[0] = p1[1]
    dx1 = x1[1] - x1[0]
    dx2 = x2[1] - x2[0]

    for i in range(1, len(y1)):
        y1[i] = y1[i-1] + m*dx1

    for i in range(1, len(x2)):
        y2[i] = y2[i-1] + m*dx2

    y1 = [int(round(y)) for y in y1]
    y2 = [int(round(y)) for y in y2]

    pos_points = [(x1[i], y1[i]) for i in range(0, len(x1))]
    neg_points = [(x2[i], y2[i]) for i in range(0, len(x2))]
    return pos_points, neg_points


def skin_algo_one(img, point):
    # Returns T/F for if there is skin at a pixel

    B, G, R = (img[point[0], point[1]])
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    H, S, V = (hsv[point[0], point[1]])
    S = S/255
    if H >= 0 and H <= 50:
        pass
    else:
        return False
    if S >= 0.23 and S <= 0.68:
        pass
    else:
        return False
    if R > 95 and G > 40 and B > 20:
        pass
    else:
        return False
    if R > G and R > B:
        pass
    else:
        return False
    if abs(R-G) > 15:
        return True
    else:
        return False


def skin_mask(img):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    bgr = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)

    r, c, _ = img.shape

    for j in range(0, c):
        for i in range(0, r):
            B, G, R = (bgr[i, j])
            H, S, V = (hsv[i, j])
            S = S/255

            if H >= 0 and H <= 50:
                pass
            else:
                bgr[i, j] = [0, 0, 0]
            if S >= 0.23 and S <= 0.68:
                pass
            else:
                bgr[i, j] = [0, 0, 0]
            if R > 95 and G > 40 and B > 20:
                pass
            else:
                bgr[i, j] = [0, 0, 0]
            if R > G and R > B:
                pass
            else:
                bgr[i, j] = [0, 0, 0]
            if abs(R-G) > 15:
                pass
            else:
                bgr[i, j] = [0, 0, 0]
    return bgr


def skin_mask2(img):
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    min_HSV = np.array([0, 58, 30], dtype="uint8")
    max_HSV = np.array([33, 255, 255], dtype="uint8")
    skinRegionHSV = cv2.inRange(hsv, min_HSV, max_HSV)
    skinHSV = cv2.bitwise_and(img, img, mask=skinRegionHSV)
    return skinHSV


def outline_hand(img, masked):
    # Outlines hand from image where the skin mask was applied
    grayscale = cv2.cvtColor(masked, cv2.COLOR_BGR2GRAY)
    _, binary = cv2.threshold(grayscale, 50, 255, cv2.THRESH_BINARY)
    contours, _ = cv2.findContours(
        binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

    # Remove contours which are not large enough
    try:
        contours = [x for x in contours if cv2.contourArea(x) > 2000]
        cv2.drawContours(img, contours, -1, (255, 0, 0), 2)
    except cv2.error as e:
        error = ErrorCode.HAND_NOT_FOUND
        print("Error:",error.name)
        return error
    return img


def get_wrist_width(img, points, pixels_per_cm, skin_mask, draw=False):
    img = outline_hand(img, skin_mask)

    r, c, _ = img.shape
    base_of_wrist = points[0]
    middle_knuckle = points[9]
    pos, neg = normal_points(
        base_of_wrist, middle_knuckle, base_of_wrist, int(round(c/2)))
    point_pos = pos[0]
    pos_truth = True
    i = 0
    while pos_truth == True:
        i += 1
        point_pos = pos[i]

        B, _, _ = img[point_pos[1], point_pos[0]]

        if B >= 250:
            pos_truth = False
        else:
            pos_truth = True

    point_neg = neg[0]
    neg_truth = True
    i = 0
    while neg_truth == True:
        i += 1
        point_neg = neg[i]

        B, _, _ = img[point_neg[1], point_neg[0]]

        if B >= 250:
            neg_truth = False
        else:
            neg_truth = True

    dist_pixels = euclidean(point_pos, point_neg)
    dist_cm = dist_pixels / pixels_per_cm

    if draw == True:
        cv2.circle(img, point_pos, 5, (0, 255, 0),
                   thickness=5, lineType=cv2.FILLED)

        cv2.circle(img, point_neg, 5, (0, 255, 0),
                   thickness=5, lineType=cv2.FILLED)

        cv2.line(img, point_neg, point_pos,
                 (0, 150, 0), 1, lineType=cv2.LINE_AA)
    return [point_neg, point_pos]
    return dist_cm, img, [point_neg, point_pos]


def get_knuckles_width(img, points, pixels_per_cm, draw=False):
    left_knuckle = points[17]
    right_knuckle = points[5]
    dydx = (left_knuckle[1]-right_knuckle[1]) / \
        (left_knuckle[0]-right_knuckle[1])
    right_truth = True
    right_point = right_knuckle
    it = 0
    while right_truth == True:
        it += 1
        B, G, R = img[right_point[1], right_point[0]]
        if B > 250:
            right_truth = False

        right_point = (int(right_knuckle[0] + it),
                       int(round(right_knuckle[1]+it*dydx)))

    left_point = left_knuckle
    left_truth = True
    it = 0
    while left_truth == True:
        it += 1
        B, G, R = img[left_point[1], left_point[0]]
        if B > 250:
            left_truth = False

        left_point = (int(left_knuckle[0] - it),
                      int(round(left_knuckle[1]-it*dydx)))

    dist_pixels = euclidean(left_point, right_point)
    dist_cm = dist_pixels / pixels_per_cm

    if draw == True:
        cv2.circle(img, right_point, 5, (0, 255, 0),
                   thickness=5, lineType=cv2.FILLED)

        cv2.circle(img, left_point, 5, (0, 255, 0),
                   thickness=5, lineType=cv2.FILLED)

        cv2.line(img, left_point, right_point,
                 (0, 150, 0), 1, lineType=cv2.LINE_AA)

    # print([left_point, right_point])
    return [left_point, right_point]
    # return dist_cm, img


def get_index_finger_width(img, points, pixels_per_cm, draw=False):
    main_knuckle = points[6]
    top_knuckle = points[8]
    pos, neg = normal_points(main_knuckle, top_knuckle, main_knuckle, 100)
    pos_point = main_knuckle
    truth = True
    it = 0
    while truth == True:
        B, G, R = img[pos_point[1], pos_point[0]]
        if B > 252:
            truth = False
        else:
            it += 1
            pos_point = pos[it]

    neg_point = neg[0]
    truth = True
    it = 0
    while truth == True:
        B, G, R = img[neg_point[1], neg_point[0]]
        if B > 252:
            truth = False
        else:
            it += 1
            neg_point = neg[it]
    if draw == True:
        cv2.circle(img, pos_point, 2, (0, 255, 0),
                   thickness=5, lineType=cv2.FILLED)
        cv2.circle(img, neg_point, 2, (0, 255, 0),
                   thickness=5, lineType=cv2.FILLED)
        cv2.line(img, pos_point, neg_point,
                 (0, 150, 0), 1, lineType=cv2.LINE_AA)

    dist_pixels = euclidean(neg_point, pos_point)
    dist_cm = dist_pixels / pixels_per_cm

    return [neg_point, pos_point]


def get_middle_finger_width(img, points, pixels_per_cm, draw=False):
    main_knuckle = points[10]
    top_knuckle = points[12]
    pos, neg = normal_points(main_knuckle, top_knuckle, main_knuckle, 100)
    pos_point = main_knuckle
    truth = True
    it = 0
    while truth == True:
        B, G, R = img[pos_point[1], pos_point[0]]
        if B > 252:
            truth = False
        else:
            it += 1
            pos_point = pos[it]

    neg_point = neg[0]
    truth = True
    it = 0
    while truth == True:
        B, G, R = img[neg_point[1], neg_point[0]]
        if B > 252:
            truth = False
        else:
            it += 1
            neg_point = neg[it]
    if draw == True:
        cv2.circle(img, pos_point, 2, (0, 255, 0),
                   thickness=5, lineType=cv2.FILLED)
        cv2.circle(img, neg_point, 2, (0, 255, 0),
                   thickness=5, lineType=cv2.FILLED)
        cv2.line(img, pos_point, neg_point,
                 (0, 150, 0), 1, lineType=cv2.LINE_AA)

    dist_pixels = euclidean(neg_point, pos_point)
    dist_cm = dist_pixels / pixels_per_cm

    return [neg_point, pos_point]


def get_ring_finger_width(img, points, pixels_per_cm, draw=False):
    main_knuckle = points[14]
    top_knuckle = points[16]
    pos, neg = normal_points(main_knuckle, top_knuckle, main_knuckle, 100)
    pos_point = main_knuckle
    truth = True
    it = 0
    while truth == True:
        B, G, R = img[pos_point[1], pos_point[0]]
        if B > 252:
            truth = False
        else:
            it += 1
            pos_point = pos[it]

    neg_point = neg[0]
    truth = True
    it = 0
    while truth == True:
        B, G, R = img[neg_point[1], neg_point[0]]
        if B > 252:
            truth = False
        else:
            it += 1
            neg_point = neg[it]
    if draw == True:
        cv2.circle(img, pos_point, 2, (0, 255, 0),
                   thickness=5, lineType=cv2.FILLED)
        cv2.circle(img, neg_point, 2, (0, 255, 0),
                   thickness=5, lineType=cv2.FILLED)
        cv2.line(img, pos_point, neg_point,
                 (0, 150, 0), 1, lineType=cv2.LINE_AA)

    dist_pixels = euclidean(neg_point, pos_point)
    dist_cm = dist_pixels / pixels_per_cm

    return [neg_point, pos_point]


def get_pinkie_finger_width(img, points, pixels_per_cm, draw=False):
    main_knuckle = points[18]
    top_knuckle = points[20]
    pos, neg = normal_points(main_knuckle, top_knuckle, main_knuckle, 100)
    pos_point = main_knuckle
    truth = True
    it = 0
    while truth == True:
        B, G, R = img[pos_point[1], pos_point[0]]
        if B > 252:
            truth = False
        else:
            it += 1
            pos_point = pos[it]

    neg_point = neg[0]
    truth = True
    it = 0
    while truth == True:
        B, G, R = img[neg_point[1], neg_point[0]]
        if B > 252:
            truth = False
        else:
            it += 1
            neg_point = neg[it]
    if draw == True:
        cv2.circle(img, pos_point, 2, (0, 255, 0),
                   thickness=5, lineType=cv2.FILLED)
        cv2.circle(img, neg_point, 2, (0, 255, 0),
                   thickness=5, lineType=cv2.FILLED)
        cv2.line(img, pos_point, neg_point,
                 (0, 150, 0), 1, lineType=cv2.LINE_AA)

    dist_pixels = euclidean(neg_point, pos_point)
    dist_cm = dist_pixels / pixels_per_cm

    return [neg_point, pos_point]


def get_thumb_finger_width(img, points, pixels_per_cm, draw=False):
    main_knuckle = points[3]
    top_knuckle = points[4]
    pos, neg = normal_points(main_knuckle, top_knuckle, main_knuckle, 100)
    pos_point = main_knuckle
    truth = True
    it = 0
    while truth == True:
        B, G, R = img[pos_point[1], pos_point[0]]
        if B > 252:
            truth = False
        else:
            it += 1
            pos_point = pos[it]

    neg_point = neg[0]
    truth = True
    it = 0
    while truth == True:
        B, G, R = img[neg_point[1], neg_point[0]]
        if B > 252:
            truth = False
        else:
            it += 1
            neg_point = neg[it]
    if draw == True:
        cv2.circle(img, pos_point, 2, (0, 255, 0),
                   thickness=5, lineType=cv2.FILLED)
        cv2.circle(img, neg_point, 2, (0, 255, 0),
                   thickness=5, lineType=cv2.FILLED)
        cv2.line(img, pos_point, neg_point,
                 (0, 150, 0), 1, lineType=cv2.LINE_AA)

    dist_pixels = euclidean(neg_point, pos_point)
    dist_cm = dist_pixels / pixels_per_cm

    return [neg_point, pos_point]


def get_widths(img, points, pixels_per_cm, skin_mask, draw=False):
    try:
        # Returns a dictinary withh width values
        widths = {}
        widths['wrist'] = get_wrist_width(
            img, points, pixels_per_cm, skin_mask, draw)
        # widths['wrist'], _, _ = get_wrist_width(
        #     img, points, pixels_per_cm, skin_mask, draw)
        widths['knuckles'] = get_knuckles_width(
            img, points, pixels_per_cm, draw)
        # widths['knuckles'], _ = get_knuckles_width(
        #     img, points, pixels_per_cm, draw)
        widths['index_finger_width'] = get_index_finger_width(   
            img, points, pixels_per_cm, draw)
        # widths['index_finger_width'], _ = get_index_finger_width(
        #     img, points, pixels_per_cm, draw)
        widths['middle_finger_width'] = get_middle_finger_width(
            img, points, pixels_per_cm, draw)
        widths['pinky_width'] = get_pinkie_finger_width(
            img, points, pixels_per_cm, draw)
        widths['thumb_width'] = get_thumb_finger_width(
            img, points, pixels_per_cm, draw)
        widths['ring_finger_width'] = get_ring_finger_width(
            img, points, pixels_per_cm, draw)
    except KeyError as e:
        error = ErrorCode.HAND_KEYPOINTS_ERROR
        print("Error:",error.name)
        return error
    except cv2.error as e:
        error = ErrorCode.UNCATEGORIZED_CV2_ERROR
        print("Error:",error.name)
        return error
    except Exception as e:
        error = ErrorCode.UNCATEGORIZED_ERROR
        print("Error:",error.name)
        return error
    return widths


def finger_max_widths(points, pixels_per_cm, img):
    thumb_pts = points[2:5]
    index_finger_pts = points[5:9]
    middle_finger_pts = points[9:13]
    ring_finger_pts = points[13:17]
    pinkie_pts = points[17:21]

    # Estimates base of finger as 3/4 dist b/w bottom 2 knuckles
    # In future could find real base by finding the point when the width greatly increases
    for finger in [thumb_pts, index_finger_pts, middle_finger_pts, ring_finger_pts, pinkie_pts]:
        finger[0] = (int(round((finger[1][0]*1.5+finger[0][0]*0.5)*0.5)),
                     int(round((finger[1][1]*1.5+finger[0][1]*0.5)*0.5)))

    for finger in [thumb_pts, index_finger_pts, middle_finger_pts, ring_finger_pts, pinkie_pts]:
        max_width = 0
        for i in range(0, len(finger)-1):
            bw = generate_pts(finger[i], finger[i+1], 100)
            for j in range(0, len(bw)-1):
                pos_norm, neg_norm = normal_points(
                    finger[i], finger[i+1], bw[j], 50)
                for point in pos_norm:
                    cv2.circle(img, point, 1, (0, 0, 255),
                               thickness=1, lineType=cv2.FILLED)
                for point in neg_norm:
                    cv2.circle(img, point, 1, (255, 0, 0),
                               thickness=1, lineType=cv2.FILLED)
                pos_truth = [skin_algo_one(img, point) for point in pos_norm]
                neg_truth = [skin_algo_one(img, point) for point in neg_norm]
    return


def snap_points(frame, points):
    # Snaps knuckle keypoints to center of finger
    pairs = [[18, 19], [14, 15], [10, 11], [6, 7], [3, 4]]
    try:
        for pair in pairs:
            lower_point = points[pair[0]]
            upper_point = points[pair[1]]
            pos_low, neg_low = normal_points(
                lower_point, upper_point, lower_point, 400)
            pos_up, neg_up = normal_points(
                upper_point, lower_point, upper_point, 400)
            j = 0
            endpts = [0, 0, 0, 0]
            for vector in [pos_low, neg_low, pos_up, neg_up]:
                i = 0
                point = vector[i]
                truth = False
                while truth == False:
                    B, G, R = frame[point[1], point[0]]
                    if B > 252:
                        truth = True
                    else:
                        i += 1
                        point = vector[i]
                endpts[j] = point
                j += 1
            new_lower = midpoint(endpts[0], endpts[1])
            new_lower = (int(new_lower[0]), int(new_lower[1]))
            new_upper = midpoint(endpts[2], endpts[3])
            new_upper = (int(new_upper[0]), int(new_upper[1]))
            points[pair[0]] = new_lower
            points[pair[1]] = new_upper
    except KeyError as e:
        error = ErrorCode.HAND_KEYPOINTS_ERROR
        print("Error:",error.name)
        return error
    except cv2.error as e:
        error = ErrorCode.UNCATEGORIZED_CV2_ERROR
        print("Error:",error.name)
        return error
    return points


def generate_pts(p1, p2, n):
    # Generates discrete points between two points
    xvec = np.linspace(p1[0], p2[0], n)
    yvec = np.linspace(p1[1], p2[1], n)
    pts = [(int(round(xvec[i])), int(round(yvec[i])))
           for i in range(0, len(xvec))]
    return pts


def get_fingers(frame, points, pixels_per_cm, skin_mask, draw=True):
    # Gets finger lengths and dist from palm base to finger base
    try:
        knuckle_width = get_knuckles_width(frame, points, pixels_per_cm)
        knuckle_width = euclidean(knuckle_width[0], knuckle_width[1])
        # knuckle_width = knuckle_width*pixels_per_cm
        finger_names = ['pinky_length', 'ring_finger',
                        'middle_finger', 'index_finger', 'thumb']
        pairs = [[17, 18], [13, 14], [9, 10], [5, 6]]
        fingers = []
        lengths = []
        for pair in pairs:
            base_knuckle = points[pair[0]]
            finger_knuckle = points[pair[1]]
            top_knuckle = points[pair[1]+1]
            pts = generate_pts(base_knuckle, finger_knuckle, 100)
            bw = generate_pts(finger_knuckle, top_knuckle, 100)
            dx = top_knuckle[0] - finger_knuckle[0]
            dy = top_knuckle[1] - finger_knuckle[1]
            bw_len = len(bw)
            for i in range(0, len(bw)):
                bw.append((bw[i][0]+dx, bw[i][1]+dy))
            for i in range(bw_len, len(bw)):
                bw.append((bw[i][0]+dx, bw[i][1]+dy))
            edge = False
            j = 0
            while edge == False:
                checkpt = bw[j]

                B, G, R = frame[checkpt[1], checkpt[0]]
                if B > 252:
                    edge = True
                else:
                    j += 1
            if draw == True:
                cv2.circle(frame, checkpt, 3, (0, 0, 255), thickness=3)
            widthit = knuckle_width + 1
            for pt in pts:
                pos, neg = normal_points(pt, finger_knuckle, base_knuckle, 500)
                completion = [0, 0]
                i = 0
                while completion[0] == 0:
                    posit = pos[i]

                    B, G, R = frame[posit[1], posit[0]]
                    if B > 252:
                        completion[0] = 1
                        i = 0
                    else:
                        i += 1
                while completion[1] == 0:
                    negit = neg[i]

                    # cv2.circle(frame, negit, 3, (0,0,255))
                    # cv2.imshow('win', frame)
                    # cv2.waitKey(0)
                    # cv2.destroyAllWindows()


                    B, G, R = frame[negit[1], negit[0]]
                    if B > 252:
                        # cv2.circle(frame, negit, 3, (0,0,255))
                        # cv2.imshow('win', frame)
                        # cv2.waitKey(0)
                        # cv2.destroyAllWindows()
                        completion[1] = 1
                        i = 0
                    else:
                        i += 1

                widthit = euclidean(negit, posit)

                if widthit < (0.3*knuckle_width):
                    break
            finger_length = euclidean(pt, checkpt) / pixels_per_cm
            # lengths.append(finger_length)
            lengths.append([checkpt, pt])
            if draw == True:
                cv2.circle(frame, pt, 3, (0, 0, 255), thickness=3)
                cv2.line(frame, checkpt, pt, (0, 100, 200), thickness=1)

            fingers.append(checkpt)
            fingers.append(pt)
        thumb_base = points[2]
        thumb_tip = points[4]
        bw = generate_pts(thumb_base, thumb_tip, 100)
        dx = thumb_tip[0] - thumb_base[0]
        dy = thumb_tip[1] - thumb_base[1]
        for i in range(0, len(bw)):
            bw.append((bw[i][0]+dx, bw[i][1]+dy))
        edge = False
        j = 0
        while edge == False:
            checkpt = bw[j]
            B, G, R = frame[checkpt[1], checkpt[0]]
            if B > 252:
                edge = True
            else:
                j += 1
        finger_length = euclidean(thumb_base, checkpt) / pixels_per_cm
        # lengths.append(finger_length)
        lengths.append([thumb_base, checkpt])
        if draw == True:
            cv2.circle(frame, thumb_base, 3, (0, 0, 255), thickness=3)
            cv2.circle(frame, checkpt, 3, (0, 0, 255), thickness=3)
            cv2.line(frame, thumb_base, checkpt, (0, 100, 200), thickness=1)
        fingers.append(thumb_base)
        fingers.append(checkpt)

        data = {}
        i = 0
        for name in finger_names:
            data[name] = lengths[i]
            i += 1

        finger_web = []
        j = 0
        for i in range(1, len(fingers)-3, 2):
            finger_web.append(midpoint(fingers[i], fingers[i+2]))
            finger_web[j] = (int(round(finger_web[j][0])),
                            int(round(finger_web[j][1])))
            cv2.circle(frame, finger_web[j], 3, (220, 220, 0), 3)
            j += 1

        wrist = get_wrist_width(frame, points, pixels_per_cm, skin_mask)
        bw = generate_pts(wrist[0], wrist[1], 100)

        web_base = []
        for pt in finger_web:
            shortest = 10000
            for point in bw:
                dist = euclidean(pt, point)
                if dist < shortest:
                    shortest_pt = point
                    shortest = dist
            cv2.circle(frame, shortest_pt, 3, (220, 220, 0), 3)
            web_base.append(shortest_pt)

        for i in range(0, len(finger_web)):
            cv2.line(frame, finger_web[i], web_base[i], (220, 220, 0), thickness=1)

        i = 0
        for name in ['palm_to_pinky_ring', 'palm_to_ring_middle', 'palm_to_middle_index']:
            # data[name] = euclidean(web_base[i], finger_web[i]) / pixels_per_cm
            data[name] = [web_base[i], finger_web[i]]
            i += 1
    except KeyError as e:
        error = ErrorCode.HAND_KEYPOINTS_ERROR
        print("Error:",error.name)
        return error
    except cv2.error as e:
        error = ErrorCode.UNCATEGORIZED_CV2_ERROR
        print("Error:",error.name)
        return error
    except Exception as e:
        error = ErrorCode.UNCATEGORIZED_ERROR
        print("Error:",error.name)
        return error
    return data


def draw_sample_measurements(image, points, measurements):
    freedom_units = dict()
    for name in measurements:
        freedom_units[name] = measurements[name] / 2.54

    cv2.putText(image, str(round(freedom_units['middle_finger'], 2))+' in', points[11],
                cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 250), 2)

    mid = midpoint(points[0], points[9])
    mid = (int(round(mid[0])), int(round(mid[1])))
    cv2.putText(image, str(round(freedom_units['palm_to_ring_middle'], 2))+' in', mid,
                cv2.FONT_HERSHEY_SIMPLEX, 0.6, (220, 220, 0), 2)

    cv2.putText(image, str(round(freedom_units['wrist'], 2))+' in', points[0],
                cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 250, 0), 2)

    cv2.putText(image, str(round(freedom_units['knuckles'], 2))+' in', points[13],
                cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 250, 0), 2)
    return image
