import pymongo as pm
from pymongo import MongoClient
import time
from settings import Hooks
import cv2
import base64

class Entry:
    def __init__(self, key, dbName, collectionName, user):
        try:
            self.client = MongoClient(key)
            self.db = self.client[dbName]
            self.collection = self.db[collectionName]
            self.init_post = {'_id': user+time.strftime("%Y%m%d-%H%M%S")}
            self.collection.insert_one(self.init_post)
            self.data = {}
            self.err = Hooks()
            self.err = self.err.database
        except Exception as e:
            return self.err

    def add_measurements(self, measurement_dict):
        try:
            post = self.init_post.copy()
            post.update(measurement_dict)
            self.data.update(post)
            self.collection.update(self.init_post, post)
        except Exception as e:
            print('err')
            return self.err

    def record_error(self, error):
        try:
            post = error 
            self.data.update(post)
            self.collection.update(self.init_post, self.data)
        except Exception as e:
            print('err')
            return self.err
            

    def add_original_img_error(self, path):
        # Should change temp to something unique per instance then delete it
        # cv2.imwrite('temp.jpg', img)
        with open(path, 'rb') as imageFile:
            encoded = base64.b64encode(imageFile.read())
            post = {'original': encoded}
            self.data.update(post)
            self.collection.update(self.init_post, self.data)


    def add_original_img(self, img):
        # Should change temp to something unique per instance then delete it
        cv2.imwrite('temp.jpg', img)
        with open('temp.jpg', 'rb') as imageFile:
            encoded = base64.b64encode(imageFile.read())
            post = {'original': encoded}
            self.data.update(post)
            self.collection.update(self.init_post, self.data)

    def add_annotated_img(self,img):
        # Should change temp to something unique per instance then delete it
        cv2.imwrite('temp.jpg', img)
        with open('temp.jpg', 'rb') as imageFile:
            encoded = base64.b64encode(imageFile.read())
            post = {'annotated': encoded}
            self.data.update(post)
            self.collection.update(self.init_post, self.data)


if __name__ == '__main__':
    secret = 'mongodb+srv://gohara:1234@cluster0.hnle2.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'
    db_name = 'hands'
    collection_name = 'hands'
    user = 'gohara'
    db_entry = Entry(secret, db_name, collection_name, user)
    db_entry.add_measurements({'finger': 30, 'palm': 20})
    img = cv2.imread('hand.jpg')
    db_entry.add_original_img(img)
