import json
import numpy as np
from imutils import contours
import finger_widths as fw
import movable_points as mp
import scaling_object as so
import cv2
import os

def get_measurements(filename, scaled_height=800, scaling_object_length=8.56, scaling_object_width=5.397, imshow=False):

    # Returns measurement data in json format

    scaled_frame = so.resize(filename, scaled_height)
    original_img = scaled_frame[:]

    hand_keypoints = mp.get_hp_data_fromimg(scaled_frame)
    hand_keypoints = mp.point_indexing(hand_keypoints)

    scaling_object_corners = so.get_scaling_object(scaled_frame)

    # skin_mask = fw.skin_mask(original_img)
    skin_mask = fw.skin_mask2(original_img)

    annotated_img = fw.outline_hand(scaled_frame, skin_mask)

    hand_keypoints = fw.snap_points(annotated_img, hand_keypoints)

    cm_per_pixel, _ = so.get_scale(
        annotated_img, scaling_object_corners, scaling_object_length, scaling_object_width)

    pixel_per_cm = 1/cm_per_pixel

    annotated_img, measurement_data = fw.get_fingers(
        annotated_img, hand_keypoints, pixel_per_cm, skin_mask, True)

    width_measurements = fw.get_widths(
        annotated_img, hand_keypoints, pixel_per_cm, skin_mask, True)

    for name in width_measurements:
        measurement_data[name] = width_measurements[name]

    json_data = json.dumps(measurement_data)

    annotated_img = so.draw_box(annotated_img, scaling_object_corners)

    fw.draw_sample_measurements(
        annotated_img, hand_keypoints, measurement_data)

    if imshow == True:
        cv2.imshow('im', annotated_img)
        cv2.waitKey(0)
        cv2.destroyAllWindows()
    # img_path = os.path.dirname(filename)
    # cv2.imwrite(os.path.join(img_path,'outline.jpg'),annotated_img)

    return json_data



if __name__ == '__main__':
    data = get_measurements('hand.jpg', imshow=True)
    print(data)
