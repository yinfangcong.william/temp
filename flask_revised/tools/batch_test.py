import numpy as np
import pandas as pd 
import os
import main_functions as mf
import json
import datetime as dt


def batch_test(folder, data_file, output_filename):
    cwd = os.getcwd()
    images_path = cwd+"/"+folder
    image_names = os.listdir(images_path)
    output_data = pd.DataFrame()
    print(image_names)

    for name in image_names:
        pth = images_path+"/"+name
        print(pth)
        try:
            data = json.loads(mf.get_measurements(pth))
            df = pd.DataFrame(data, index=[name])
            output_data = output_data.append(df)
        except:
            pass


    output_data.to_csv(output_filename)





if __name__ == '__main__':
    folder = 'test_images'
    batch_test(folder, '', 'test.csv')



        
