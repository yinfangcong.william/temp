from scipy.spatial.distance import euclidean
import cv2
import numpy as np


def get_hp_data(photo_file=str, threshold=0.1):

    # Returns hand keypoints using a tensorflow model
    protoFile = "pose_deploy.prototxt"
    weightsFile = "pose_iter_102000.caffemodel"
    nPoints = 22

    frame = cv2.imread(photo_file)
    frameCopy = np.copy(frame)

    frameWidth = frame.shape[1]
    frameHeight = frame.shape[0]
    aspect_ratio = frameWidth/frameHeight

    inHeight = 368
    inWidth = int(((aspect_ratio*inHeight)*8)//8)

    net = cv2.dnn.readNetFromCaffe(protoFile, weightsFile)

    inpBlob = cv2.dnn.blobFromImage(frame, 1.0 / 255, (inWidth, inHeight),
                                    (0, 0, 0), swapRB=False, crop=False)

    net.setInput(inpBlob)
    output = net.forward()

    points = []

    for i in range(nPoints):
        # confidence map of corresponding body's part.
        probMap = output[0, i, :, :]
        probMap = cv2.resize(probMap, (frameWidth, frameHeight))

        # Find global maxima of the probMap.
        minVal, prob, minLoc, point = cv2.minMaxLoc(probMap)

        if prob > threshold:
            cv2.circle(frameCopy, (int(point[0]), int(
                point[1])), 6, (0, 255, 255), thickness=-1, lineType=cv2.FILLED)
            cv2.putText(frameCopy, "{}".format(i), (int(point[0]), int(
                point[1])), cv2.FONT_HERSHEY_SIMPLEX, .8, (0, 0, 255), 2, lineType=cv2.LINE_AA)

            # Add the point to the list if the probability is greater than the threshold
            points.append((int(point[0]), int(point[1])))
        else:
            points.append(None)

    return points


def draw_skeleton(photo_file, output_filename, points, pixel_per_cm):
    # Draws a hand skeleton witht the keypoints
    POSE_PAIRS = [[0, 1], [1, 2], [2, 3], [3, 4], [0, 5], [5, 6], [6, 7], [7, 8], [0, 9], [9, 10], [
        10, 11], [11, 12], [0, 13], [13, 14], [14, 15], [15, 16], [0, 17], [17, 18], [18, 19], [19, 20]]
    frame = cv2.imread(photo_file)
    # Draw Skeleton
    for pair in POSE_PAIRS:
        partA = pair[0]
        partB = pair[1]

        if points[partA] and points[partB]:
            cv2.line(frame, points[partA], points[partB],
                     (0, 255, 0), 5, lineType=cv2.LINE_AA)
            cv2.circle(frame, points[partA], 5, (0, 0, 255),
                       thickness=40, lineType=cv2.FILLED)
            cv2.circle(frame, points[partB], 5, (0, 0, 255),
                       thickness=40, lineType=cv2.FILLED)
            len = euclidean(points[partA], points[partB]) / pixel_per_cm
            mid_pt = ((points[partA][0] + points[partB][0]) / 2,
                      (points[partA][1] + points[partB][1]) / 2)
            cv2.putText(frame, "{:.2f}cm".format(len), (int(mid_pt[0]), int(
                mid_pt[1])), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0), 2)

    cv2.imwrite(output_filename, frame)

    return frame


def get_closest_point(points, x, y):
    index_pos = [i for i in range(len(points)) if points[i]]
    points = [point for point in points if point]
    closest = points[0]
    shortest = 10000
    iterindex = 0
    closest_index = 0
    for point in points:
        distit = ((x-point[0])**2 + (y-point[1])**2)**0.5
        if distit < shortest:
            closest = point
            shortest = distit
            closest_index = iterindex
        iterindex += 1
    closest_index = index_pos[closest_index]
    return closest_index, shortest


def get_hp_data_fromimg(frame, threshold=0.1):

    # Returns hand keypoints using a tensorflow model
    protoFile = "pose_deploy.prototxt"
    weightsFile = "pose_iter_102000.caffemodel"
    nPoints = 22

    frameCopy = np.copy(frame)

    frameWidth = frame.shape[1]
    frameHeight = frame.shape[0]
    aspect_ratio = frameWidth/frameHeight

    inHeight = 368
    inWidth = int(((aspect_ratio*inHeight)*8)//8)

    net = cv2.dnn.readNetFromCaffe(protoFile, weightsFile)

    inpBlob = cv2.dnn.blobFromImage(frame, 1.0 / 255, (inWidth, inHeight),
                                    (0, 0, 0), swapRB=False, crop=False)

    net.setInput(inpBlob)
    output = net.forward()

    points = []

    for i in range(nPoints):
        # confidence map of corresponding body's part.
        probMap = output[0, i, :, :]
        probMap = cv2.resize(probMap, (frameWidth, frameHeight))

        # Find global maxima of the probMap.
        minVal, prob, minLoc, point = cv2.minMaxLoc(probMap)

        if prob > threshold:
            cv2.circle(frameCopy, (int(point[0]), int(
                point[1])), 6, (0, 255, 255), thickness=-1, lineType=cv2.FILLED)
            cv2.putText(frameCopy, "{}".format(i), (int(point[0]), int(
                point[1])), cv2.FONT_HERSHEY_SIMPLEX, .8, (0, 0, 255), 2, lineType=cv2.LINE_AA)

            # Add the point to the list if the probability is greater than the threshold
            points.append((int(point[0]), int(point[1])))
        else:
            points.append(None)

    return points


def draw_skeleton_fromimg(frame, points, pixel_per_cm):
    # Draws a hand skeleton witht the keypoints
    POSE_PAIRS = [[0, 1], [1, 2], [2, 3], [3, 4], [0, 5], [5, 6], [6, 7], [7, 8], [0, 9], [9, 10], [
        10, 11], [11, 12], [0, 13], [13, 14], [14, 15], [15, 16], [0, 17], [17, 18], [18, 19], [19, 20]]

    measurements = [None for pair in POSE_PAIRS]

    # Draw Skeleton
    i = 0
    for pair in POSE_PAIRS:
        partA = pair[0]
        partB = pair[1]

        if points[partA] and points[partB]:
            cv2.line(frame, points[partA], points[partB],
                     (0, 255, 0), 1, lineType=cv2.LINE_AA)
            cv2.circle(frame, points[partA], 5, (0, 0, 255),
                       thickness=1, lineType=cv2.FILLED)
            cv2.circle(frame, points[partB], 5, (0, 0, 255),
                       thickness=1, lineType=cv2.FILLED)
            length = euclidean(points[partA], points[partB]) / pixel_per_cm
            measurements[i] = length
            mid_pt = ((points[partA][0] + points[partB][0]) / 2,
                      (points[partA][1] + points[partB][1]) / 2)
            cv2.putText(frame, "{:.2f}cm".format(length), (int(mid_pt[0]), int(
                mid_pt[1])), cv2.FONT_HERSHEY_SIMPLEX, 0.4, (0, 0, 0), 1)
        i += 1

    return frame, measurements


def point_indexing(points):
    # Corrects indexing of hand keypoints according to finger
    points_original = points[:]
    points_out = points[:]
    finger_tips = [4, 8, 12, 16, 20]
    hand = str

    # Designate hand as R/L
    if points_original[4][0] > points_original[0][0]:
        hand = 'left'
    else:
        hand = 'right'

    # order the fingers left to right
    x_vals = [(points_original[index][0], index) for index in finger_tips]
    x_vals = sorted(x_vals, key=lambda x: x[0])
    left_to_right_index = [x[1] for x in x_vals]
    if hand == 'left':
        j = 0
        for i in range(20, 3, -4):
            for k in range(0, 4):
                points_out[i-k] = points_original[left_to_right_index[j]-k]
            j += 1
    if hand == 'right':
        j = 0
        for i in range(4, 21, 4):
            for k in range(0, 4):
                points_out[i-k] = points_original[left_to_right_index[j]-k]
            j += 1
    print(hand)
    return points_out
