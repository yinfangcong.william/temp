from timeit import Timer
import cv2
import numpy as np
import imutils
import main as bg
from imutils import contours
from scipy.spatial.distance import euclidean
from imutils import perspective
import math
import hand_keypoints as hp
import scaling_object as so
import measurement_detection as md
import math_functions as mf
import json
from settings import Hooks
from error_code import ErrorCode


class Hand:
    def __init__(self, filename, scaled_height=800, uncertainty_bound=0.05, card_height=8.57, card_width=5.397):
        self.hooks = Hooks()
        self.filename = filename
        self.resized = self.resize(scaled_height)
        # self.resized = so.resize(filename, scaled_height)
        self.height, self.width, _ = np.shape(self.resized)
        self.annotated = self.resized.copy()
        self.card_height = card_height
        self.card_width = card_width

        # self.keypoints = hp.get_hp_data_fromimg(self.resized)
        # self.keypoints = hp.point_indexing(self.keypoints)

        # self.corners = so.get_scaling_object(self.resized)
        # self.annotate_scaling_object()
        # self.length_pixel_per_cm, self.width_pixel_per_cm, self.length_pts, self.width_pts = so.get_scales(
        #     self.corners)

        # cv2.imwrite('temp.jpg', self.resized)
        # bg.process('temp.jpg', 'temp.png')
        # self.background_removed = cv2.imread('temp.png')
        # md.outline_hand(self.annotated, self.background_removed)
        # self.keypoints = md.snap_points(self.annotated, self.keypoints)

        # self.finger_vectors = md.get_fingers(
        #     self.annotated, self.keypoints, 10, self.background_removed)

        # self.measurements = {}
        # for item in self.finger_vectors:
        #     self.measurements[item] = mf.adjusted_scale(
        #         self.length_pixel_per_cm, self.width_pixel_per_cm, self.length_pts, self.width_pts, self.finger_vectors[item])

        # self.width_vectors = md.get_widths(
        #     self.annotated, self.keypoints, self.length_pixel_per_cm, self.background_removed, True)

        # for item in self.width_vectors:
        #     self.measurements[item] = mf.adjusted_scale(
        #         self.length_pixel_per_cm, self.width_pixel_per_cm, self.length_pts, self.width_pts, self.width_vectors[item])

        # self.json_data = json.dumps(self.measurements)

    def annotate_hand_keypoints(self):
        hp.draw_skeleton_fromimg(self.annotated, self.keypoints)

    def resize(self, scaled_height):
        orig = cv2.imread(self.filename)
        scaled_frame = imutils.resize(orig, height=scaled_height)
        return scaled_frame

    def annotate_scaling_object(self):
        try:
            frame = cv2.GaussianBlur(self.resized, (7, 7), 0)
            edged = cv2.Canny(frame, 100, 200)
            edged = cv2.dilate(edged, None, iterations=2)
            edged = cv2.erode(edged, None, iterations=2)

            cnts = cv2.findContours(
                edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
            cnts = imutils.grab_contours(cnts)
            # sort contours from left to right as leftmost contour is reference object
            (cnts, _) = contours.sort_contours(cnts)
            # Remove contours which are not large enough
            cnts = [x for x in cnts if cv2.contourArea(x) > 2000]
            ref_object = cnts[0]

            hull = cv2.convexHull(ref_object)
            cv2.fillConvexPoly(self.annotated, hull, (200, 200, 200))
            result = so.draw_box(self.annotated, self.corners)
            if(isinstance(result,ErrorCode)):
                return self.hooks.annotation
        except Exception as e:
            return self.hooks.annotation

    def get_keypoints(self):
        try:
            self.keypoints = hp.get_hp_data_fromimg(self.resized)
            self.keypoints = hp.point_indexing(self.keypoints)
            if (self.keypoints.count(None) > 1):
                return self.hooks.keypoints
            return self.keypoints
        except:
            return self.hooks.keypoints

    def get_scaling_object(self):
        self.corners = so.get_scaling_object(self.resized)
        if(isinstance(self.corners,ErrorCode)):
            return self.hooks.annotation
        return self.corners

    def get_scales(self):
        result = so.get_scales(
            self.corners)
        #self.length_pixel_per_cm, self.width_pixel_per_cm, self.length_pts, self.width_pts = so.get_scales(
        #    self.corners)

        if(isinstance(result,ErrorCode)):
            return self.hooks.annotation
        self.length_pixel_per_cm, self.width_pixel_per_cm, self.length_pts, self.width_pts = result
        return self.length_pixel_per_cm, self.width_pixel_per_cm, self.length_pts, self.width_pts

    def remove_background(self):
        try:
            cv2.imwrite('temp.jpg', self.resized)
            bg.process('temp.jpg', 'temp.png',preprocessing_method_name='None',postprocessing_method_name='No')
            self.background_removed = cv2.imread('temp.png')
            return self.background_removed
        except Exception as e:
            print(e)
            return self.hooks.background

    def outline_hand(self):
        result = md.outline_hand(self.annotated, self.background_removed)
        return

    def center_keypoints(self):
        self.keypoints = md.snap_points(self.annotated, self.keypoints)
        return self.keypoints

    def get_finger_vectors(self):
        self.finger_vectors = md.get_fingers(
            self.annotated, self.keypoints, 10, self.background_removed)
    def get_measurements(self):
        try:
            self.measurements = {}
            for item in self.finger_vectors:
                self.measurements[item] = mf.adjusted_scale(
                    self.length_pixel_per_cm, self.width_pixel_per_cm, self.length_pts, self.width_pts, self.finger_vectors[item])

            self.width_vectors = md.get_widths(
                self.annotated, self.keypoints, self.length_pixel_per_cm, self.background_removed, True)
            if isinstance(self.width_vectors,ErrorCode):
                return self.hooks.measuremnts
            for item in self.width_vectors:
                self.measurements[item] = mf.adjusted_scale(
                    self.length_pixel_per_cm, self.width_pixel_per_cm, self.length_pts, self.width_pts, self.width_vectors[item])
            return self.measurements
        except: return self.hooks.measuremnts

    def translate_to_json(self):
        try:
            self.json_data = json.dumps(self.measurements)
            return self.json_data
        except: return self.hooks.json_translate

    # Check whether the image is too blurry to process
    # If the score is smaller than the threshold value, then it is too blurry

    @staticmethod
    def blur_detection(filename, threshold=30):
        im = cv2.imread(filename)
        gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
        try:
            score = cv2.Laplacian(im, cv2.CV_64F).var()
            if(score <= threshold):
                return True
            return False
        except:
            return True


def run_algorithm(filename):
    blur = Hand.blur_detection(filename)
    if blur == True:
        # raise Exception('101 Image too blurry')
        return {'error': 101, 'message': 'Blurry Image'}
    hand = Hand(filename)
    hooks = Hooks()
    result = hand.get_keypoints()
    if isinstance(result,dict) and 'error' in result.keys():
        return hooks.keypoints
    result = hand.get_scaling_object()
    if isinstance(result,dict) and 'error' in result.keys():
        return hooks.so
    result = hand.annotate_scaling_object()
    if isinstance(result,dict) and 'error' in result.keys():
        return hooks.annotation
    result = hand.get_scales()
    if isinstance(result,dict) and 'error' in result.keys():
        return hooks.so
    result = hand.remove_background()
    if isinstance(result,dict) and 'error' in result.keys():
        return hooks.background
    result = hand.outline_hand()
    if isinstance(result,ErrorCode):
        return hooks.keypoints
    result = hand.center_keypoints()
    if isinstance(result,ErrorCode):
        return hooks.keypoints
    result = hand.get_finger_vectors()
    if isinstance(result,ErrorCode):
        return hooks.measurements
    result = hand.get_measurements()
    if isinstance(result,dict) and 'error' in result.keys():
        return result
    
    hand.translate_to_json()
    
    return hand


if __name__ == '__main__':
    filename = 'something.jpg'

    hand = run_algorithm(filename)
    print(hand)
    print(hand.measurements)
    # hand = Hand(filename)
    # hand.get_keypoints()
    # hand.get_scaling_object()
    # hand.annotate_scaling_object()
    # hand.get_scales()
    # hand.remove_background()
    # hand.outline_hand()
    # hand.center_keypoints()
    # hand.get_finger_vectors()
    # hand.get_measurements()
    # hand.translate_to_json()

    # hand.keypoints = Hand.get_keypoints(hand.resized)
    # hand.corners = Hand.get_scaling_object(hand.resized)
    # hand.annotate_scaling_object()
    # hand.length_pixel_per_cm, hand.width_pixel_per_cm, hand.length_pts, hand.width_pts = hand.get_scales(hand.corners)
    # print(hand.length_pixel_per_cm)
    # hand.background_removed = hand.remove_background(hand.resized)
    # hand.keypoints = hand.center_keypoints(hand.annotated, hand.keypoints)
    # hand.finger_vectors = hand.get_finger_vectors(hand.keypoints, hand.background_removed)

    # cv2.imshow('win', hand.annotated)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

