@app.route('/api/data/measure/',methods=["GET"])
def getAllMeasurements():
    result = dict()
    try:
        num_match,data,features_list = db.query_all(return_features=False)
        if num_match!=0:
            result_data = dict()
            for i in range(len(data)):
                for key in data[i].keys():
                    if (isinstance(data[i][key],bytes)):
                        data[i][key] = base64.b64encode(data[i][key]).decode('utf-8')
                    if (isinstance(data[i][key],bson.ObjectId)):
                        data[i][key] = str(data[i][key])
                result_data[i] = data[i]
            result['data'] = result_data
        result['num_match'] = num_match
        result_str = json.dumps(result) 
        return jsonify(result)
    except Exception as ex:
        return jsonify({'error':str(ex)})
#Query measurements for a given name
@app.route('/api/data/measure/<name>',methods=["GET"])
def getMeasurementsByName(name):
    result = dict()
    try:
        #num_match,data = db.query_data_by_feature('Name',name,exact=False)
        num_match,data = db.query_data_by_feature('_id',name,exact=False)
        if num_match!=0:
            result_data = dict()
            for i in range(len(data)):
                for key in data[i].keys():
                    if (isinstance(data[i][key],bytes)):
                        data[i][key] = base64.b64encode(data[i][key]).decode('utf-8')
                    if (isinstance(data[i][key],bson.ObjectId)):
                        data[i][key] = str(data[i][key])
                result_data[i] = data[i]
            result['data'] = result_data
        result['num_match'] = num_match
        result_str = json.dumps(result) 
        return jsonify(result)
    except Exception as ex:
        return jsonify({'error':str(ex)})

#Query all hand measurements
@app.route('/api/data/measure/hand/',methods=["GET"])
def getAllHandMeasurements():
    result = dict()
    try:
        num_match,data,features_list = db.query_all(return_features=False)
        hand_feature_names = ['pinky_length', 'ring_finger', 'middle_finger', 'index_finger', 'thumb', 'palm_to_pinky_ring', 'palm_to_ring_middle', 'palm_to_middle_index', 'wrist', 'knuckles', 'index_finger_width', 'middle_finger_width', 'pinky_width', 'thumb_width', 'ring_finger_width']

        if num_match!=0:
            result_data = dict()
            for i in range(len(data)):
                hand_item = dict()
                hand_item['_id'] = str(data[i]['_id'])
                #hand_item['Name'] = data[i]['Name']
                if 'fail' in data[i].keys():
                    hand_item['fail'] = 1
                else:
                    hand_item['fail'] = 0
                for key in hand_feature_names:
                    if key in data[i].keys():
                        hand_item[key] = data[i][key]
                    else:
                        hand_item[key] = None
                result_data[i] = hand_item
            result['data'] = result_data
        result['num_match'] = num_match
        result_str = json.dumps(result) 
        return jsonify(result)
    except Exception as ex:
        return jsonify({'error':str(ex)})
#Query hand measurements by name
@app.route('/api/data/measure/hand/<name>',methods=["GET"])
def getHandMeasurementsByName(name):
    result = dict()
    try:
        #num_match,data = db.query_data_by_feature('Name',name,exact=False)
        num_match,data = db.query_data_by_feature('_id',name,exact=False)
        hand_feature_names = ['pinky_length', 'ring_finger', 'middle_finger', 'index_finger', 'thumb', 'palm_to_pinky_ring', 'palm_to_ring_middle', 'palm_to_middle_index', 'wrist', 'knuckles', 'index_finger_width', 'middle_finger_width', 'pinky_width', 'thumb_width', 'ring_finger_width']

        if num_match!=0:
            result_data = dict()
            for i in range(len(data)):
                hand_item = dict()
                hand_item['_id'] = str(data[i]['_id'])
                #hand_item['Name'] = data[i]['Name']
                if 'fail' in data[i].keys():
                    hand_item['fail'] = 1
                else:
                    hand_item['fail'] = 0
                for key in hand_feature_names:
                    if key in data[i].keys():
                        hand_item[key] = data[i][key]
                    else:
                        hand_item[key] = None
                result_data[i] = hand_item
            result['data'] = result_data
        result['num_match'] = num_match
        result_str = json.dumps(result) 
        return jsonify(result)
    except Exception as ex:
        return jsonify({'error':str(ex)})
#Query original image by name
@app.route('/api/data/measure/OriginalImage/<name>',methods=["GET"])
def getOrigImageByName(name):
    result = dict()
    try:
        #num_match,data = db.query_data_by_feature('Name',name,exact=False)
        num_match,data = db.query_data_by_feature('_id',name,exact=False)
        if num_match!=0:
            result_data = dict()
            for i in range(len(data)):
                hand_item = dict()
                hand_item['_id'] = str(data[i]['_id'])
                #hand_item['Name'] = data[i]['Name']
                if 'fail' in data[i].keys():
                    hand_item['fail'] = 1
                else:
                    hand_item['fail'] = 0
                key = 'original'
                if (key in data[i].keys()):
                    #hand_item[key] = data[i][key] = base64.b64encode(data[i][key]).decode('utf-8')
                    hand_item[key] = base64.b64encode(data[i][key]).decode('utf-8')
                else:
                    hand_item[key] = None
                result_data[i] = hand_item
            result['data'] = result_data
        result['num_match'] = num_match
        result_str = json.dumps(result) 
        return jsonify(result)
    except Exception as ex:
        return jsonify({'error':str(ex)})
#Query original image by name
@app.route('/api/data/measure/AnnotatedImage/<name>',methods=["GET"])
def getAnnotatedImageByName(name):
    result = dict()
    try:
        #num_match,data = db.query_data_by_feature('Name',name,exact=False)
        num_match,data = db.query_data_by_feature('_id',name,exact=False)
        if num_match!=0:
            result_data = dict()
            for i in range(len(data)):
                hand_item = dict()
                hand_item['_id'] = str(data[i]['_id'])
                #hand_item['Name'] = data[i]['Name']
                if 'fail' in data[i].keys():
                    hand_item['fail'] = 1
                else:
                    hand_item['fail'] = 0
                key = 'annotated'
                if (key in data[i].keys()):
                    hand_item[key] = data[i][key] = base64.b64encode(data[i][key]).decode('utf-8')
                    #hand_item[key] = data[i][key]
                    
                else:
                    hand_item[key] = None
                result_data[i] = hand_item
            result['data'] = result_data
        result['num_match'] = num_match
        result_str = json.dumps(result) 
        return jsonify(result)
    except Exception as ex:
        return jsonify({'error':str(ex)})
if __name__ == '__main__':
    
    app.run(debug=True,threaded=True)
