import cv2
import numpy as np
from numpy.core.fromnumeric import shape
import scaling_object as so
from imutils import perspective
from imutils import contours
import imutils

# filename = 'angled.jpeg'

# image = so.resize(filename, 800)

# card_corners = so.get_scaling_object(image)
# image_corners = np.float32([[0, 0], [image.shape[1], 0], [
#                            image.shape[1], image.shape[0]],  [0, image.shape[0]]])

# # card_corners = np.float32(card_corners)

# # print(image_corners)
# # print(image.shape)
# # print(card_corners)

# frame = so.draw_box(image, card_corners)
# # frame = so.draw_box(frame, image_corners)

# # transform = cv2.getPerspectiveTransform(card_corners, image_corners)
# # print(transform)

# # frame = cv2.warpPerspective(image, transform, (image.shape[1], image.shape[0]))


# cv2.imshow('frame', frame)
# cv2.waitKey(0)
# cv2.destroyAllWindows()




# def get_scaling_object(frame):
#    # Finds rectangular scaling object corners
    
#     # Smoothen image, other options are commented out

#     # frame = cv2.blur(frame, (5,5))
#     frame = cv2.GaussianBlur(frame, (7,7), 0)
#     # frame = cv2.bilateralFilter(frame, 9, 75, 75)

#     edged = cv2.Canny(frame, 100, 200)
#     edged = cv2.dilate(edged, None, iterations=1)
#     edged = cv2.erode(edged, None, iterations=1)
    

#     cnts = cv2.findContours(
#         edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
#     cnts = imutils.grab_contours(cnts)

#     # sort contours from left to right as leftmost contour is reference object
#     (cnts, _) = contours.sort_contours(cnts)

#     # Remove contours which are not large enough
#     cnts = [x for x in cnts if cv2.contourArea(x) > 2000]


#     for c in cnts:
#         peri = cv2.arcLength(c, True)
#         approx = cv2.approxPolyDP(c, 0.02*peri, True)
#         if len(approx) == 4 :
#             cv2.drawContours(image, [approx], -1, (0,255,0), 3)
#             ref_object = c 
#             print(approx)
#             break
    


#     return
        

    # # reference object dimensions (credit card, 5.397 x 8.56 cm)
    # ref_object = cnts[0]






def get_scaling_object(frame):

    # Finds rectangular scaling object corners
    
    # Smoothen image, other options are commented out

    # frame = cv2.blur(frame, (5,5))
    frame = cv2.GaussianBlur(frame, (7,7), 0)
    # frame = cv2.bilateralFilter(frame, 9, 75, 75)

    edged = cv2.Canny(frame, 100, 200)
    edged = cv2.dilate(edged, None, iterations=1)
    edged = cv2.erode(edged, None, iterations=1)
    

    cnts = cv2.findContours(
        edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    cnts = imutils.grab_contours(cnts)

    # sort contours from left to right as leftmost contour is reference object
    (cnts, _) = contours.sort_contours(cnts)

    # Remove contours which are not large enough
    cnts = [x for x in cnts if cv2.contourArea(x) > 2000]


    hull = [cv2.convexHull(cnts[0])]
    cv2.drawContours(frame, hull, 0, (255,0,0))




    epsilon = 0.02 * cv2.arcLength(hull[0], True)
    approx_corners = cv2.approxPolyDP(hull[0], epsilon, True)
    cv2.drawContours(frame, approx_corners, -1, (255, 255, 0), 10)
    approx_corners = sorted(np.concatenate(approx_corners).tolist())
    corners = [approx_corners[i] for i in [0, 2, 1, 3]]

    (tl, tr, br, bl) = corners
    corners = [tl, tr, br, bl]
    corners = [(corner[0], corner[1]) for corner in corners]

    cv2.imshow('frame', frame)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


    im = four_point_transform(frame, np.array(corners))

    cv2.imshow('frame', im)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


    corners = order_points(np.array(corners))
    # box = np.array(corners)

    # box = perspective.order_points(box)
    # (tl, tr, br, bl) = box

    corners = [tl, tr, br, bl]
    corners = [(corner[0], corner[1]) for corner in corners]
    print(corners)



    # print('\nThe corner points are ...\n')
    # for index, c in enumerate(approx_corners):
    #     character = chr(65 + index)
    #     print(character, ':', c)
    #     cv2.putText(frame, character, tuple(c), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2, cv2.LINE_AA)

    

    # cv2.imshow('frame', frame)
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()

    return corners


def four_point_transform(image, pts):
    # obtain a consistent order of the points and unpack them
    # individually
    rect = order_points(pts)
    print(rect)
    (tl, tr, br, bl) = rect
    # compute the width of the new image, which will be the
    # maximum distance between bottom-right and bottom-left
    # x-coordiates or the top-right and top-left x-coordinates
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))
    # compute the height of the new image, which will be the
    # maximum distance between the top-right and bottom-right
    # y-coordinates or the top-left and bottom-left y-coordinates
    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))
    # now that we have the dimensions of the new image, construct
    # the set of destination points to obtain a "birds eye view",
    # (i.e. top-down view) of the image, again specifying points
    # in the top-left, top-right, bottom-right, and bottom-left
    # order
    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype = "float32")
    # compute the perspective transform matrix and then apply it
    M = cv2.getPerspectiveTransform(rect, dst)

    imHeight, imWidth, _ = image.shape
    print(imHeight)
    print(imWidth)
    tlTran = (round(imWidth*0.167), round(imHeight*0.167))
    trTran = (round(tlTran[0]+maxWidth), tlTran[1])
    blTran = (tlTran[0], round(tlTran[1] - maxHeight))
    brTran = (trTran[0], blTran[1])

    cornersTransform = np.array([tlTran, trTran, brTran, blTran])
    cornersTransform = np.array([blTran, brTran, trTran, tlTran])
    print(cornersTransform)

    h, status = cv2.findHomography(rect, cornersTransform)
    warped = cv2.warpPerspective(image, h, (round(imWidth*1.25), round(imHeight*1.25)))
    # warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))
    # return the warped image
    return warped



def order_points(pts):
	# initialzie a list of coordinates that will be ordered
	# such that the first entry in the list is the top-left,
	# the second entry is the top-right, the third is the
	# bottom-right, and the fourth is the bottom-left
	rect = np.zeros((4, 2), dtype = "float32")
	# the top-left point will have the smallest sum, whereas
	# the bottom-right point will have the largest sum
	s = pts.sum(axis = 1)
	rect[0] = pts[np.argmin(s)]
	rect[2] = pts[np.argmax(s)]
	# now, compute the difference between the points, the
	# top-right point will have the smallest difference,
	# whereas the bottom-left will have the largest difference
	diff = np.diff(pts, axis = 1)
	rect[1] = pts[np.argmin(diff)]
	rect[3] = pts[np.argmax(diff)]
	# return the ordered coordinates
	return rect



def get_destination_points(corners):
    """
    -Get destination points from corners of warped images
    -Approximating height and width of the rectangle: we take maximum of the 2 widths and 2 heights
    Args:
        corners: list
    Returns:
        destination_corners: list
        height: int
        width: int
    """

    w1 = np.sqrt((corners[0][0] - corners[1][0]) ** 2 + (corners[0][1] - corners[1][1]) ** 2)
    w2 = np.sqrt((corners[2][0] - corners[3][0]) ** 2 + (corners[2][1] - corners[3][1]) ** 2)
    w = max(int(w1), int(w2))

    h1 = np.sqrt((corners[0][0] - corners[2][0]) ** 2 + (corners[0][1] - corners[2][1]) ** 2)
    h2 = np.sqrt((corners[1][0] - corners[3][0]) ** 2 + (corners[1][1] - corners[3][1]) ** 2)
    h = max(int(h1), int(h2))

    destination_corners = np.float32([(0, 0), (w - 1, 0), (0, h - 1), (w - 1, h - 1)])
    
    # print('\nThe destination points are: \n')
    # for index, c in enumerate(destination_corners):
    #     character = chr(65 + index) + "'"
    #     print(character, ':', c)
        
    # print('\nThe approximated height and width of the original image is: \n', (h, w))
    return destination_corners


def unwarp(img, src, dst):
    """
    Args:
        img: np.array
        src: list
        dst: list
    Returns:
        un_warped: np.array
    """
    h, w = img.shape[:2]
    H, _ = cv2.findHomography(src, dst, method=cv2.RANSAC, ransacReprojThreshold=3.0)
    un_warped = cv2.warpPerspective(img, H, (w, h), flags=cv2.INTER_LINEAR)
    return un_warped




frame = so.resize('hand.jpg', 800)
corners = get_scaling_object(frame)



# destination_corners = get_destination_points(corners)
# corners = np.array(corners)
# im = unwarp(frame, corners, destination_corners)
# cv2.imshow('im', im)
# cv2.waitKey(0)
# cv2.destroyAllWindows()
# print(destination_corners)



frame = so.resize('IMG_1546.JPG', 800)
get_scaling_object(frame)