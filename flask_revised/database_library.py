import pymongo
from pymongo import MongoClient
from PIL import Image
from bson.binary import Binary
from matplotlib import pyplot as plt
import io
import pandas as pd
import cv2
import base64
import json
class database:

    def __init__(self):
        db_name = 'hands'
        connection_str ='mongodb+srv://gohara:1234@cluster0.hnle2.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'
        self.client = pymongo.MongoClient(connection_str)
        self.db = self.client[db_name]
        self.collection = self.db['hands']

    def insert_from_json(self,data):
        data_id = self.collection.insert_one(data).inserted_id
        return data_id
    
    def query_data_by_criteria(self,criteria):
        coll = self.collection
        data  = list()
        try:
            cursor = coll.find(criteria)
            for item in cursor:
                data.append(dict(item))
        except Exception as ex:
            data.append({'error':str(ex)})
        return len(data),data

    def query_data_by_feature(self,feature_name,feature_value,exact=True,return_features=None):
        criteria = {feature_name:feature_value}
        if feature_name == None:
            criteria = None
        elif(not exact):
            criteria[feature_name] = {'$regex':'^.*{}.*'.format(feature_value)}
        num_match,data = self.query_data_by_criteria(criteria)
        if(num_match==0):
            return num_match,None
        return_data = list()
        if return_features:
            for item in data:
                return_item = dict()
                for key in item.keys():
                    if key in return_features:
                        return_item[key] = item[key]
                return_data.append(return_item)
        else:
            return_data = data
        return num_match,return_data
    def query_all(self,return_features=False):
        num_match,data = self.query_data_by_feature(None,None,exact=True)
        features_list=None
        if(num_match==0):
            return num_match,None,features_list
        if return_features:
            features_list = [feature for feature in data[0].keys()]   
        return num_match,data,features_list
class Base64Encoder(json.JSONEncoder):
    # pylint: disable=method-hidden
    def default(self, o):
        if isinstance(o, bytes):
            return b64encode(o).decode()
        return json.JSONEncoder.default(self, o)

if __name__ == '__main__':
    db_handler = database()
    
    num_match,data,flist = db_handler.query_all(return_features=True)
    #print(num_match,'items found in the database!')
    
    if(num_match!=0):
        print(flist)
    for item in data:
        #print(item['_id'])
        print(item['annotated'])
        '''
        s = {'a':base64.b64encode(item['annotated']).decode('utf-8')}
        st = s['a'].encode('utf-8')
        st =base64.b64decode(st)
        '''
        pil_img = Image.open(io.BytesIO(item['annotated']))
        plt.imshow(pil_img)
        plt.show()
    
    '''
    while(ctrl!='q'):
        print('---- Start query ----')
        feature_name = input('Please enter a feature name that you want to perform the query ')
        feature_value = input('Please enter the value of the feature ')
        if('_' in feature_name):
            feature_value = float(feature_value)
        exact = int(input('Do you want to match exactly? Yes-1 No-0 '))
        if(exact==1):
            exact = True
        else:
            exact = False
        num_match,data = db_handler.query_data_by_feature(feature_name,feature_value,exact=False)
        print(num_match,'items found in the database!')
        if(num_match!=0):
            for item in data:
                print('Name:',item['Name'])
                print(feature_name,':',item[feature_name])
                try:
                    
                    pil_img = Image.open(io.BytesIO(item['AnnotatedImage']))
                    plt.imshow(pil_img)
                    plt.show()
                except:
                    print('No annotated image found!')
                print()
        ctrl = input('--- Enter q to end the query or any other key to start a new query ----')
        '''
    
    
        
