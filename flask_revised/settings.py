class Hooks:
    def __init__(self):
        self.blurry = {'error': 101, 'message': 'Blurry Image'}
        self.annotation = {'error': 102, 'message': 'Card Annotation Failed'}
        self.keypoints =  {'error': 103, 'message': 'Not all keypoints detected'}
        self.background  = {'error': 104, 'message': 'Failed to remove background'}
        self.measuremnts = {'error': 105, 'message': 'Failed to translate vectors to measurement values'}
        self.json_translate = {'error': 106, 'message': 'Failed to translate measurements to json'}
        self.cv2_error = {'error':107,'message':'uncategorized cv2 error'}
        self.other_error = {'error':108,'message':'uncategorized errors'}
        self.database = {'error': 109, 'message': 'failed to write to database'}
        self.so = {'error': 110, 'message': 'Failed to locate scaling object'}
        self.measuremnts = {'error': 111, 'message': 'Failed to get measurements'}


        
