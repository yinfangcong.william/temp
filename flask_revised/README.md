# Custom Glove Project API service and backend program

This folder includes the custom glove project API service 

The service is already deployed on an AWS instance. The root url is the following:
http://ec2-54-162-80-191.compute-1.amazonaws.com

# What's here

The API service is in `app.py`.

The backend hand detection algorithm is run in `main_functions.py`.

The hand detections are run in `finger_widths.py` and `movable_points.py`.


# Prerequisite

## Algorithm Dependencies

Please refer to `dependencies.txt` file for all dependencies to install for this project.

## API Dependencies

For dependencies related to API deployment on the server, please refer to the following tutorial:

https://medium.com/faun/deploy-flask-app-with-nginx-using-gunicorn-7fda4f50066a

# API Modules

## API Components 

See the following swagger documentation for details of all methods included in this API:

https://app.swaggerhub.com/apis/mim-test/CustomGloveAPI/0.1

## How to Deploy on AWS Server

Please follow the following tutorial to deploy the app to your server:

https://medium.com/faun/deploy-flask-app-with-nginx-using-gunicorn-7fda4f50066a

For practical reasons, the header `access-control-allow-origin` in the nginx config file on the current running server is set to be all sites. For security and stability, it would be better to restrict the header `access-control-allow-origin` in the production version of the API.

