from scipy.spatial.distance import euclidean
from imutils import perspective
from imutils import contours
import imutils
import cv2
from error_code import ErrorCode

def resize(frame_file, scaled_height):
    try:
        orig = cv2.imread(frame_file)
        scaled_frame = imutils.resize(orig, height=scaled_height)
    except:
        error = ErrorCode.INVALID_IMAGE
        print("Error:",error.name)
        return error
    return scaled_frame


def get_scaling_object(frame):
    # Finds rectangular scaling object corners
    
    # Smoothen image, other options are commented out

    # frame = cv2.blur(frame, (5,5))
    frame = cv2.GaussianBlur(frame, (7,7), 0)
    # frame = cv2.bilateralFilter(frame, 9, 75, 75)

    edged = cv2.Canny(frame, 100, 200)
    edged = cv2.dilate(edged, None, iterations=1)
    edged = cv2.erode(edged, None, iterations=1)
    
    try:
        cnts = cv2.findContours(
            edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
    except Exception as e:
        error = ErrorCode.SCALING_OBJECT_NOT_FOUND
        print("Error:",error.name)
        return error
    # sort contours from left to right as leftmost contour is reference object
    (cnts, _) = contours.sort_contours(cnts)

    # Remove contours which are not large enough
    cnts = [x for x in cnts if cv2.contourArea(x) > 2000]

    # reference object dimensions (credit card, 5.397 x 8.56 cm)
    try:
        ref_object = cnts[0]
        box = cv2.minAreaRect(ref_object)
        box = cv2.boxPoints(box)
        box = perspective.order_points(box)
        (tl, tr, br, bl) = box
    except IndexError as e:
        error = ErrorCode.SCALING_OBJECT_NOT_FOUND
        print("Error:",error.name)
        return error
   

    corners = [tl, tr, br, bl]
    corners = [(corner[0], corner[1]) for corner in corners]

    return corners




def measurements_on_box(img, corners, pixel_per_cm):
    pairs = [[0, 1], [0, 3], [2, 1], [2, 3]]
    pair_distances = [
        euclidean(corners[pair[0]], corners[pair[1]]) for pair in pairs]
    midpoints = [midpoint(corners[pair[0]], corners[pair[1]]) for pair in pairs]
    for i in range(0,3):
        cv2.putText(img, pair_distances[i], midpoints[i], cv2.FONT_HERSHEY_SIMPLEX, 1, (255,0,0))



def get_scale(frame, corners, scale_length=8.56, scale_width=5.397):
    # Length > Width
    # Returns scale in units per pixel
    pairs = [[0, 1], [0, 3], [2, 1], [2, 3]]
    pair_distances = [
        euclidean(corners[pair[0]], corners[pair[1]]) for pair in pairs]
    scale = scale_length / max(pair_distances)
    return scale, pair_distances


def get_scales(corners, scale_length=8.56, scale_width=5.397):
    pairs = [[0, 1], [0, 3], [2, 1], [2, 3]]
    try:
        pair_distances = [
            euclidean(corners[pair[0]], corners[pair[1]]) for pair in pairs]
        pair_distances.sort()
        for pair in pairs:
            if euclidean(corners[pair[0]], corners[pair[1]]) == pair_distances[2]:
                length_pts = [corners[pair[0]], corners[pair[1]]]
            if euclidean(corners[pair[0]], corners[pair[1]]) == pair_distances[0]:
                width_pts = [corners[pair[0]], corners[pair[1]]]
    except IndexError as e:
        error = ErrorCode.SCALING_OBJECT_NOT_FOUND
        print("Error:",error.name)
        return error
    except Exception as e:
        error = ErrorCode.UNCATEGORIZED_ERROR
        print("Error:",error.name)
        return error
    if(scale_length==0 or scale_width==0):
        error = ErrorCode.ARITHMETIC_ERROR
        print("Error:",error.name)
        return error
    length_scale = pair_distances[2]/ scale_length
    width_scale = pair_distances[0] / scale_width
    return length_scale, width_scale, length_pts, width_pts

def get_closest_point(points, x, y):
    index_pos = [i for i in range(len(points)) if points[i]]
    points = [point for point in points if point]
    closest = points[0]
    shortest = 10000
    iterindex = 0
    closest_index = 0
    for point in points:
        distit = ((x-point[0])**2 + (y-point[1])**2)**0.5
        if distit < shortest:
            closest = point
            shortest = distit
            closest_index = iterindex
        iterindex += 1
    closest_index = index_pos[closest_index]
    return closest_index, shortest


def midpoint(p1, p2):
    return ((p1[0]+p2[0])/2, (p1[1]+p2[1])/2)


def draw_distances(frame, corners, scale_length, scale_width, units):
    # Annotates image with scaling object dimensions
    fontFace = cv2.FONT_HERSHEY_SIMPLEX
    fontScale = 0.5
    color = (255, 255, 0)
    thickness = 2
    pairs = [[0, 1], [0, 3], [2, 1], [2, 3]]
    scale, distances = get_scale(frame, corners, scale_length, scale_width)
    scaled_distances = [round(distance*scale, 2) for distance in distances]
    midpoints = [midpoint(corners[pair[0]], corners[pair[1]])
                 for pair in pairs]
    midpoints = [(int(point[0]), int(point[1])) for point in midpoints]
    i = int(0)
    for point in midpoints:
        cv2.putText(frame, str(
            scaled_distances[i]) + units, point, fontFace, fontScale, color, thickness)
        i += 1
    return frame


def draw_box(frame, corners):
    pairs = [[0, 1], [0, 3], [2, 1], [2, 3]]
    
    for pair in pairs:
        try:
            partA = pair[0]
            partB = pair[1]
            cv2.line(frame, corners[partA], corners[partB],
                        (0, 255, 0), 1, lineType=cv2.LINE_AA)
            cv2.circle(frame, corners[partA], 5, (0, 0, 255),
                        thickness=1, lineType=cv2.FILLED)
            cv2.circle(frame, corners[partB], 5, (0, 0, 255),
                        thickness=1, lineType=cv2.FILLED)
        except IndexError as e:
            error = ErrorCode.SCALING_OBJECT_NOT_FOUND
            print("Error:",error.name)
            return error, None
    return frame

def fill_ref_object(img):
        # frame = img.copy()
        frame = cv2.GaussianBlur(img, (7, 7), 0)

        edged = cv2.Canny(frame, 100, 200)
        edged = cv2.dilate(edged, None, iterations=2)
        edged = cv2.erode(edged, None, iterations=2)

        cnts = cv2.findContours(
            edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = imutils.grab_contours(cnts)
        # sort contours from left to right as leftmost contour is reference object
        (cnts, _) = contours.sort_contours(cnts)
        # Remove contours which are not large enough
        cnts = [x for x in cnts if cv2.contourArea(x) > 2000]
        ref_object = cnts[0]

        hull = cv2.convexHull(ref_object)
        cv2.fillConvexPoly(img, hull, (200, 200, 200))

if __name__ == "__main__":
    filename = 'hand2.jpg'
    #If there is no error when running each of the following functions, the returned result_code will be 0
    #Otherwise, the corresponding error code should be returned and the error will be printed
    result_code,frame = resize(filename, 800)
    result_code,corners = get_scaling_object(frame)
    
    result_code,_ = draw_box(frame, corners)
    result_code,_,_,_,_ = get_scales(corners)
